# Frontend Mentor & Codecademy - Advice generator app & Mixed Messages solution

This is a solution to the [Advice generator app challenge on Frontend 
Mentor](https://www.frontendmentor.io/challenges/advice-generator-app-QdUG-13db). 
Frontend Mentor challenges to help me build realistic projects, plus practice Mixed
Messages project to practice coding in JavaScript.

## Table of contents

- [Overview](#overview)
  - [The challenge](#the-challenge)
  - [Screenshot](#screenshot)
  - [Links](#links)
- [My process](#my-process)
  - [Built with](#built-with)
  - [What I learned](#what-i-learned)
  - [Useful resources](#useful-resources)
- [Author](#author)


## Overview

### The challenge

Users should be able to:

- View the optimal layout for the app depending on their device's screen 
size
- See hover states for all interactive elements on the page
- Generate a new piece of advice by clicking the dice icon
- Get advise that should be pull from an API, found on RapidAPI's website.

### Screenshot

![](./images/screenshot-1.png)

### Links

- Live Site URL: [The live site](https://shanirj-portfolio.gitlab.io/advise-generator-app/)

## My process

### Built with

- Semantic HTML5 markup
- CSS custom properties
- Flexbox
- Mobile-first workflow
- Javascript

### What I learned

I learned how to add an custom action/function to a button:

```html
<button id="random-btn" onclick="fetchData()"><img src="/images/icon-dice.svg" alt=""></button>
```

I also learned how to fetch data from an API and update the content using a HTML elements' 
id: 
```js
let scripture = {};
let topic = '';
let context = '';

// call fetch when page loads
document.addEventListener("DOMContentLoaded", function() {
  fetchData();
});


const options = {
  method: 'GET',
  headers: {
    'X-RapidAPI-Key': 'myKey',
    'X-RapidAPI-Host': 'uncovered-treasure-v1.p.rapidapi.com'
  }
};

function fetchData() {
  fetch('https://uncovered-treasure-v1.p.rapidapi.com/random', options)
    .then(response => response.json())
    .then(response => {
      scripture = response;
      topic = scripture.results[0].text;
      context = scripture.results[0].context;
      document.getElementById("topic").innerHTML = topic;
      document.getElementById("verses").innerHTML = context;
    })
    .catch(err => { console.error(err); });
}
```

### Useful resources

- [RapidAPI](https://www.rapidapi.com) - This website really helped me understand how to 
bring in the data and I learned a lot about fetch().

## Author

- Website - [My website](https://www.shanirivers.com)
- Frontend Mentor - 
[@shanirivers](https://www.frontendmentor.io/profile/shanirivers)
- Twitter - [@shani_codes](https://www.twitter.com/shani_codes)


let scripture = {};
let topic = '';
let context = '';

// call fetch when page loads
document.addEventListener("DOMContentLoaded", function() {
  fetchData();
});


const options = {
  method: 'GET',
  headers: {
    'X-RapidAPI-Key': '8aacb59881msh045494cc0b11b43p14c448jsnfc23b6342115',
    'X-RapidAPI-Host': 'uncovered-treasure-v1.p.rapidapi.com'
  }
};

function fetchData() {
  fetch('https://uncovered-treasure-v1.p.rapidapi.com/random', options)
    .then(response => response.json())
    .then(response => {
      scripture = response;
      topic = scripture.results[0].text;
      context = scripture.results[0].context;
      document.getElementById("topic").innerHTML = topic;
      document.getElementById("verses").innerHTML = context;
    })
    .catch(err => { console.error(err); });
}